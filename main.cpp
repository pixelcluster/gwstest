#include <iostream>

#include "/usr/include/libdrm/amdgpu.h"
#include "/usr/include/libdrm/amdgpu_drm.h"
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <vector>
#include <array>
#include <cassert>
#include "amdgfxregs.h"
#include "sid.h"

#define SOPP(op, imm) (0b10111111100000000000000000000000 | (op << 16) | (imm))
#define OP_S_CODE_END 31

#define DIV_ROUND_UP(A, B)  ( ((A) + (B) - 1) / (B) )

#define HANDLE_POSIX_ERROR(error, msg) \
if (error) { \
   std::cerr << msg": " << strerror(-(error)) << "\n"; \
   throw error; \
}

class AMDGPUBO;

class PKT3CommandBuffer;

class AMDGPUDevice {
public:
   AMDGPUDevice();

   ~AMDGPUDevice();

   AMDGPUBO allocateBO(uint64_t size, uint32_t domain);

   void submitAndWait(const PKT3CommandBuffer &buffer, const std::vector<const AMDGPUBO*>& bufferList);

   void waitFences();

private:
   amdgpu_device_handle m_device;
   amdgpu_context_handle m_context;

   std::vector<amdgpu_cs_fence> m_fences;
   std::vector<amdgpu_bo_list_handle> m_boLists;
};

class AMDGPUBO {
public:
   AMDGPUBO(amdgpu_device_handle deviceHandle, uint64_t size, uint32_t domain);

   AMDGPUBO(const AMDGPUBO &) = delete;

   AMDGPUBO &operator=(const AMDGPUBO &) = delete;

   AMDGPUBO(AMDGPUBO &&) = default;

   AMDGPUBO &operator=(AMDGPUBO &&) = default;

   ~AMDGPUBO();

   uint64_t va() const { return m_va; }

   void *hostPtr() const {
      return m_hostPtr;
   }

   amdgpu_bo_handle rawHandle() const { return m_bo; }

private:
   amdgpu_bo_handle m_bo;
   amdgpu_va_handle m_vaHandle = nullptr;
   uint64_t m_va = 0;
   uint64_t m_size;
   void *m_hostPtr = nullptr;
};

AMDGPUBO::AMDGPUBO(amdgpu_device_handle deviceHandle, uint64_t size, uint32_t domain) {
   uint32_t alignment;
   uint32_t flags;

   if (domain != AMDGPU_GEM_DOMAIN_GWS && domain != AMDGPU_GEM_DOMAIN_GDS) {
      m_size = (size + 4096 - 1) & 0xFFFFFFFFFFFFF000ULL;
      alignment = 4096;
      flags = 0;
   }
   else {
      m_size = size;
      alignment = 1;
      flags = AMDGPU_GEM_CREATE_NO_CPU_ACCESS;
   }

   amdgpu_bo_alloc_request req = {
           .alloc_size = m_size,
           .phys_alignment = alignment,
           .preferred_heap = domain,
           .flags = flags,
   };
      int error = amdgpu_bo_alloc(deviceHandle, &req, &m_bo);
      HANDLE_POSIX_ERROR(error, "Cannot allocate bo")

   if (domain != AMDGPU_GEM_DOMAIN_GWS && domain != AMDGPU_GEM_DOMAIN_GDS) {
      error = amdgpu_va_range_alloc(deviceHandle, amdgpu_gpu_va_range_general, m_size, 4096,
                                    0, &m_va, &m_vaHandle, 0);
      HANDLE_POSIX_ERROR(error, "Cannot allocate VA")

      error = amdgpu_bo_va_op(m_bo, 0, m_size, m_va, 0, AMDGPU_VA_OP_MAP);
      HANDLE_POSIX_ERROR(error, "Cannot map bo in GPU space")

      error = amdgpu_bo_cpu_map(m_bo, &m_hostPtr);
      HANDLE_POSIX_ERROR(error, "Cannot map bo in CPU space")
   }
}

AMDGPUBO::~AMDGPUBO() {
   int error;
   if (m_hostPtr) {
      error = amdgpu_bo_cpu_unmap(m_bo);
      HANDLE_POSIX_ERROR(error, "Cannot unmap bo in CPU space")
   }

   if (m_va) {
      error = amdgpu_bo_va_op(m_bo, 0, m_size, m_va, 0, AMDGPU_VA_OP_UNMAP);
      HANDLE_POSIX_ERROR(error, "Cannot unmap bo in GPU space")
   }

   if (m_vaHandle) {
      error = amdgpu_va_range_free(m_vaHandle);
      HANDLE_POSIX_ERROR(error, "Cannot free VA range")
   }

   error = amdgpu_bo_free(m_bo);
   HANDLE_POSIX_ERROR(error, "Cannot free bo")
}

AMDGPUDevice::AMDGPUDevice() {
   int fd = open("/dev/dri/renderD128", O_RDWR);
   if (fd == -1)
      fd = open("/dev/dri/renderD129", O_RDWR);
   if (fd == -1) {
      std::cerr << "Cannot open device files!\n";
      throw errno;
   }

   uint32_t major, minor;
   if (amdgpu_device_initialize(fd, &major, &minor, &m_device) == -1) {
      std::cerr << "Cannot init amdgpu device!\n";
      throw errno;
   }

   if (amdgpu_cs_ctx_create(m_device, &m_context) == -1) {
      std::cerr << "Cannot init amdgpu ctx!\n";
      throw errno;
   }
}

AMDGPUDevice::~AMDGPUDevice() {
   amdgpu_cs_ctx_free(m_context);
   amdgpu_device_deinitialize(m_device);
}

AMDGPUBO AMDGPUDevice::allocateBO(uint64_t size, uint32_t domain) {
   return AMDGPUBO(m_device, size, domain);
}

class PKT3CommandBuffer {
public:
   void upload(AMDGPUBO& bo) const;

   void emitPKT3(uint32_t data) { m_data.push_back(data); }

   void align();

   size_t size() const { return m_data.size(); }

   template<uint32_t Count>
   void setSHRegs(uint32_t regName, const std::array<uint32_t, Count> &data) {
      assert(regName >= SI_SH_REG_OFFSET && regName < SI_SH_REG_END);
      emitPKT3(PKT3(PKT3_SET_SH_REG, Count, 0));
      emitPKT3((regName - SI_SH_REG_OFFSET) / 4);
      for (uint32_t i = 0; i < Count; ++i) {
         emitPKT3(data[i]);
      }
   }
   template<uint32_t Count>
   void setUConfigRegs(uint32_t regName, const std::array<uint32_t, Count> &data) {
      assert(regName >= CIK_UCONFIG_REG_OFFSET && regName < CIK_UCONFIG_REG_END);
      emitPKT3(PKT3(PKT3_SET_UCONFIG_REG, Count, 0));
      emitPKT3((regName - CIK_UCONFIG_REG_OFFSET) / 4);
      for (uint32_t i = 0; i < Count; ++i) {
         emitPKT3(data[i]);
      }
   }

private:
   std::vector<uint32_t> m_data;
};

void PKT3CommandBuffer::upload(AMDGPUBO& bo) const {
   memcpy(bo.hostPtr(), m_data.data(), m_data.size() * sizeof(uint32_t));
}

void PKT3CommandBuffer::align() {
   while (m_data.empty() || m_data.size() % 16) {
      emitPKT3(PKT3(PKT3_NOP, 0, 0));
   }
}

void AMDGPUDevice::submitAndWait(const PKT3CommandBuffer &buffer, const std::vector<const AMDGPUBO*>& bufferList) {
   AMDGPUBO bo = allocateBO(buffer.size() * sizeof(uint32_t), AMDGPU_GEM_DOMAIN_VRAM);
   buffer.upload(bo);

   std::cout << "Submitting command buffer with " << buffer.size() << " dwords\n";
   std::cout << "IB VA: " << std::hex << bo.va() << "\n";

   std::vector<amdgpu_bo_handle> listHandles;
   listHandles.reserve(bufferList.size() + 1);
   for (auto& buffer : bufferList) {
      listHandles.push_back(buffer->rawHandle());
   }
   listHandles.push_back(bo.rawHandle());

   amdgpu_bo_list_handle boList;

   int error = amdgpu_bo_list_create(m_device, listHandles.size(), listHandles.data(), nullptr, &boList);
   HANDLE_POSIX_ERROR(error, "Could not allocate bo list");

   amdgpu_cs_ib_info ibInfo = {
           .ib_mc_address = bo.va(),
           .size = static_cast<uint32_t>(buffer.size())
   };
   amdgpu_cs_request request = {
           .ip_type = AMDGPU_HW_IP_GFX,
           .ring = 0,
           .resources = boList,
           .number_of_ibs = 1,
           .ibs = &ibInfo
   };
   error = amdgpu_cs_submit(m_context, 0, &request, 1);
   HANDLE_POSIX_ERROR(error, "Could not submit command buffer");

   uint32_t status, first;
   amdgpu_cs_fence fence = {
           .context = m_context,
           .ip_type = AMDGPU_HW_IP_GFX,
           .ring = 0,
           .fence = request.seq_no
   };
   error = amdgpu_cs_wait_fences(&fence, 1, true, 10000000000ull, &status, &first);
   HANDLE_POSIX_ERROR(error, "Could not wait for fences");
   if (!status) {
      std::cerr << "Waiting for fences timed out!\n";
      throw status;
   }

   bool bufferBusy;
   error = amdgpu_bo_wait_for_idle(bo.rawHandle(), 10000000000ULL, &bufferBusy);
   HANDLE_POSIX_ERROR(error, "Could not wait for bo!");

   amdgpu_bo_list_destroy(boList);

   if (bufferBusy) {
      std::cerr << "Waiting for buffer timed out!\n";
      throw bufferBusy;
   }
}

int main() {
   AMDGPUDevice device;

   uint32_t code_size;
   FILE *code_file = fopen("./asmc.hex", "r");
   fseek(code_file, 0, SEEK_END);
   code_size = (uint32_t) ftell(code_file);
   fseek(code_file, 0, SEEK_SET);

   printf("Code size: %u\n", code_size);

   assert(code_size > 0);

   AMDGPUBO shaderBO = device.allocateBO(code_size + 256, AMDGPU_GEM_DOMAIN_VRAM);
   char* code = new char[code_size];
   size_t result = fread(code, 1, code_size, code_file);
   printf("Read %zu bytes\n", result);

   assert(result == code_size);

   memcpy(shaderBO.hostPtr(), code, code_size);
   delete[] code;

   printf("Shader bo va: 0x%lx\n", shaderBO.va());

   auto *end_code = reinterpret_cast<uint32_t *>(reinterpret_cast<char *>(shaderBO.hostPtr()) + code_size);
   for (uint32_t i = 0; i < 256 / sizeof(uint32_t); ++i) {
      end_code[i] = SOPP(OP_S_CODE_END, 0);
   }

   fclose(code_file);

   AMDGPUBO gwsBO = device.allocateBO(4, AMDGPU_GEM_DOMAIN_GWS);
   printf("GWS bo va: 0x%lx\n", gwsBO.va());

   AMDGPUBO resultBO = device.allocateBO(4, AMDGPU_GEM_DOMAIN_VRAM);
   printf("Result bo va: 0x%lx\n", resultBO.va());
   *reinterpret_cast<uint32_t *>(resultBO.hostPtr()) = 0;

   PKT3CommandBuffer cmdbuf;

   /* preamble */
   cmdbuf.setSHRegs<3>(R_00B810_COMPUTE_START_X, { 0, 0, 0 });
   cmdbuf.setSHRegs<2>(R_00B858_COMPUTE_STATIC_THREAD_MGMT_SE0, {0xFFFFFFFF, 0xFFFFFFFF});
   cmdbuf.setSHRegs<2>(R_00B864_COMPUTE_STATIC_THREAD_MGMT_SE2, {0xFFFFFFFF, 0xFFFFFFFF});
   cmdbuf.setSHRegs<4>(R_00B890_COMPUTE_USER_ACCUM_0, {0, 0, 0, 0});
   cmdbuf.setSHRegs<1>(R_00B9F4_COMPUTE_DISPATCH_TUNNEL, {0});
   cmdbuf.setUConfigRegs<1>(R_0301EC_CP_COHER_START_DELAY, { S_0301EC_START_DELAY_COUNT(0x20) });
   
   cmdbuf.setUConfigRegs<1>(R_031068_GDS_GWS_RESOURCE_CNTL, { S_031068_INDEX(0) });
   cmdbuf.setUConfigRegs<1>(R_03106C_GDS_GWS_RESOURCE, { S_03106C_COUNTER(0x1) });
   
   cmdbuf.setSHRegs<2>(R_00B830_COMPUTE_PGM_LO, { static_cast<uint32_t>(shaderBO.va() >> 8), static_cast<uint32_t>(shaderBO.va() >> 40) });

   /* bind shader */
   cmdbuf.setSHRegs<2>(R_00B848_COMPUTE_PGM_RSRC1, {
           S_00B848_VGPRS(8),
           S_00B84C_USER_SGPR(2)
   });

   cmdbuf.setSHRegs<1>(R_00B8A0_COMPUTE_PGM_RSRC3, { 0 });

   cmdbuf.setSHRegs<16>(R_00B900_COMPUTE_USER_DATA_0, {
      static_cast<uint32_t>(resultBO.va()),
      static_cast<uint32_t>(resultBO.va() >> 32)
   });

   /* dispatch */
   cmdbuf.setSHRegs<3>(R_00B81C_COMPUTE_NUM_THREAD_X, {
           S_00B81C_NUM_THREAD_FULL(1),
           S_00B820_NUM_THREAD_FULL(1),
           S_00B824_NUM_THREAD_FULL(1)
   });

   /* Shader type: 0 = graphics, 1 = compute */
   cmdbuf.emitPKT3(PKT3(PKT3_DISPATCH_DIRECT, 3, false) | PKT3_SHADER_TYPE_S(1));
   cmdbuf.emitPKT3(16);
   cmdbuf.emitPKT3(16);
   cmdbuf.emitPKT3(16);
   cmdbuf.emitPKT3(S_00B800_FORCE_START_AT_000(1) | S_00B800_COMPUTE_SHADER_EN(1));

   cmdbuf.align();

   device.submitAndWait(cmdbuf, { &shaderBO, &gwsBO, &resultBO });

   printf("Success! Result buffer value: %u\n", *reinterpret_cast<uint32_t *>(resultBO.hostPtr()));
}
